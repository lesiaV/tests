//
//  City.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-05-30.
//

import Foundation

struct City {
    var id: String
    var nameCity: String
    var countryName: String
}
