//
//  WeekForecast.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-06.
//

import UIKit

enum Hours {
}

enum Week {
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday
    case sunday
    
    static var weekDays: [Week] {
         [.monday, .tuesday, .wednesday, .thursday, .friday, .saturday, .sunday]
    }
    
    static func getWeekDay(from date: Date) -> Week{
        let calendar = Calendar(identifier: .gregorian)
        let dayOfWeek = calendar.component(.weekday, from: date)
        return weekDays[dayOfWeek - 1]
    }
}

enum ForecastType {
    case weekly(Forecast)
    case daily([Week: Forecast])
}
