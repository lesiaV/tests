//
//  Forecast.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-06.
//

import UIKit

struct CityForecast {
    var idCity: String
    var startForecastDate: Date
    var forecasts: Forecast
}
