//
//  Forecast.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-06.
//

import UIKit

struct Forecast {
    var temperature: Int
    var precipProbability: Int
    var humidity: Int
    var cloudCover: Int
    var windSpeed: Int
    
    var shortDescription: String {
        return "coming soon"
    }
    
    var icon: UIImage {
        return UIImage(systemName: "sun.min")!
    }
}
