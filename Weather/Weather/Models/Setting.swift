//
//  Setting.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-07.
//

import UIKit

enum TemprecherType: Int {
    case fahrenheit
    case celsius
}

enum HourceType: Int {
    case h12
    case h24
}

struct Setting {
    var tempType: TemprecherType
    var hourceType: HourceType
}
