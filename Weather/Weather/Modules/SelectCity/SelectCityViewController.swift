//
//  SelectCityViewController.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-05-30.
//

import UIKit

class SelectCityViewController: UIViewController {
    private let selectCityCellIdn = "selectCityCellIdn"
    private let acoountVCstoryboardIdn = "UserProfileViewController"
    
    private let service = RegionService()
    private var availibleCities: [City]?
    private var selectedCity: City?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        availibleCities = service.getAvailibleCities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        switch segue.identifier {
//        case "showSettingsIdn":
//
//        default:
//            ""
//        }
//
//
//        guard let vc = segue.destination as? SettingsViewController else {
//            return
//        }
//        vc.view.setNeedsDisplay()
//        vc.delegate = self
//
//
//        guard let vc = segue.destination as? WeatherForecastViewController else {
//            return
//        }
//
//        vc.view.setNeedsDisplay()
//        vc.city = selectedCity
     }
    
    var testValue: Int = 0
    
    func getTemprecherType(isVar: Bool) {
      print("\(isVar)")
    }
    
    
    @IBSegueAction func showForecastWeather(_ coder: NSCoder, sender: Any) -> WeatherForecastViewController? {
        guard let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell)  else {
            return nil
        }
        
        let vc = WeatherForecastViewController(coder: coder, city: availibleCities![indexPath.row])
        return vc
    }
    
    @IBAction func showUserProfile(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: SettingsVC Delegate's methods
extension SelectCityViewController: SettingsViewControllerDelegate {
    func changeTemprecherType(_ type: TemprecherType) {
    }
}

// MARK: TableViewDelegate methods
extension SelectCityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selectedCity = availibleCities?[indexPath.row]
//        performSegue(withIdentifier: "showForecastCityIdn", sender: self)
    }
}

// MARK: TableViewDataSource methods
extension SelectCityViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availibleCities?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectCityCellIdn, for: indexPath)
        guard let city = availibleCities?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell.textLabel?.text = city.nameCity
        cell.detailTextLabel?.text = city.countryName
        return cell
    }
}
