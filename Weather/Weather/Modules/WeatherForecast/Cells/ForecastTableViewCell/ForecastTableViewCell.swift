//
//  ForecastTableViewCell.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-05-31.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var nameDayLabel: UILabel!
    @IBOutlet weak var forecastImageView: UIImageView!
    
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
