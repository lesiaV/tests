//
//  WeatherForecastViewController.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-05-30.
//

import UIKit

class WeatherForecastViewController: UIViewController {
    private let dailyCellIdn = "DailyCollectionViewCell"
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var forecastInfoLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var todayCollectionView: UICollectionView? {
        didSet {
            todayCollectionView?.dataSource = self
            todayCollectionView?.delegate = self
        }
    }
    
    private var city: City
    
    init?(coder: NSCoder, city: City) {
        self.city = city
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) is not implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBlur()
        registerTableViewCells()
        registerCollectionCells()
        
        navigationItem.backButtonTitle = ""
        
        cityNameLabel.text = city.nameCity
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backButtonTitle = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.backButtonTitle = ""
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func registerCollectionCells() {
 
        todayCollectionView?.register(DailyCollectionViewCell.self, forCellWithReuseIdentifier: dailyCellIdn)
        todayCollectionView?.register(UINib(nibName: "DailyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dailyCellIdn)
    }
    
    private func registerTableViewCells() {
        tableView.register(UINib(nibName: "ForecastTableViewCell", bundle: nil), forCellReuseIdentifier: "forecastCellIdn")
    }
    
    private func setupBlur() {
        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bgImageView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bgImageView.addSubview(blurEffectView)
    }
    
}

// MARK: TableViewDelegate & TableViewDataSource
extension WeatherForecastViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "", sender: self)
    }
}

extension WeatherForecastViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCellIdn") as? ForecastTableViewCell {
            return cell
        }
        
        return UITableViewCell()
    }
    
}

// MARK: CollectionView

extension WeatherForecastViewController: UICollectionViewDelegate {
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize  {
//        guard let size = todayCollectionView?.frame.size else {
//            return CGSize.zero
//        }
//        return CGSize(width: size.height / 5, height: size.height)
//    }
}

extension WeatherForecastViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: dailyCellIdn, for: indexPath) as? DailyCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.forecastIcon.image = UIImage(systemName: "sun.min")
        cell.backgroundColor = UIColor.red
        return cell
    }
}
