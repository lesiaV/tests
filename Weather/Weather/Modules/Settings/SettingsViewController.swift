//
//  SettingsViewController.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-02.
//

import UIKit

protocol SettingsViewControllerDelegate: AnyObject {
    func changeTemprecherType(_ type: TemprecherType)
}

class SettingsViewController: UIViewController {
    private let switcherCellIdn = "switcherTableViewCellIdn"
    private let defaultCellIdn = "settingsTableViewCellIdn"
    
    enum SettingsRowType {
        case cityRow
        case tempuratureType
        case hoursType
        
    }
    
    private let items: [SettingsRowType] = [.cityRow, .tempuratureType, .hoursType]
    
    weak var delegate: SettingsViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backButtonDisplayMode = .minimal
        
        tableView.register(UINib(nibName: "SwitcherTableViewCell", bundle: nil), forCellReuseIdentifier: switcherCellIdn)
        tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: defaultCellIdn)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension SettingsViewController: UITableViewDelegate {
    
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = items[indexPath.row]
        switch type {
        case .cityRow:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: defaultCellIdn) as? SettingsTableViewCell else {
                return UITableViewCell()
            }
            cell.nameRowLabel.text = "My City"
            cell.decRowLabel.text = "Lviv"
            return cell
            
        case .tempuratureType:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: switcherCellIdn) as? SwitcherTableViewCell else {
                return UITableViewCell()
            }
            cell.nameLabel.text = "Tempretyre type"
            cell.leftValueLabel.text = "C"
            cell.rightValueLabel.text = "F"
            cell.switcher.isOn = SettingsService.shared.settings.tempType == .fahrenheit
            cell.switcherValueChanged = temptChnage
            return cell
            
        case .hoursType:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: switcherCellIdn) as? SwitcherTableViewCell else {
                return UITableViewCell()
            }
            cell.nameLabel.text = "Hour type"
            cell.leftValueLabel.text = "12"
            cell.rightValueLabel.text = "24"
            cell.switcher.isOn = SettingsService.shared.settings.hourceType == .h24
            
            cell.switcherValueChanged = { isOn in
                SettingsService.shared.saveHoursType(isOn ? .h24 : .h12)
            }
            return cell
        }
    }
    
    func temptChnage(_ isOn: Bool) {
        SettingsService.shared.saveTempType(isOn ? .fahrenheit : .celsius)
    }
}
