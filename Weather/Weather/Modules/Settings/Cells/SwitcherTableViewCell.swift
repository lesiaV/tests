//
//  SwitcherTableViewCell.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-06.
//

import UIKit

class SwitcherTableViewCell: UITableViewCell {

    var switcherValueChanged: ((_ isSwitchOn: Bool) -> Void)?
    
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var leftValueLabel: UILabel!
    @IBOutlet weak var rightValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func changeValue(_ sender: Any) {
        switcherValueChanged?(switcher.isOn)
    }
}
