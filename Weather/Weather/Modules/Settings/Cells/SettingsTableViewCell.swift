//
//  SettingsTableViewCell.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-02.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var decRowLabel: UILabel!
    @IBOutlet weak var nameRowLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
         super.prepareForReuse()
         
     }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
