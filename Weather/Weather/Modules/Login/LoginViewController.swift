//
//  LoginViewController.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-05.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var pswTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton? {
        didSet {
            loginButton?.layer.cornerRadius = 16
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
