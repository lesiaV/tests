//
//  SettingsService.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-07.
//

import UIKit

class SettingsService {
    private enum Keys: String {
        case settingsHours = "settingsHoursKey"
        case settingsTempt = "settingsTempKey"
    }
    
    private init() {
        settings = Setting(tempType: .celsius, hourceType: .h24)
        loadSettings()
    }
    
    static let shared = SettingsService()
    
    private(set) var settings: Setting
    
    func saveTempType(_ type: TemprecherType) {
        settings.tempType = type
        storeSettings()
    }
    
    func saveHoursType(_ type: HourceType) {
        settings.hourceType = type
        storeSettings()
    }
    
    // MARK:
    private func storeSettings() {
        UserDefaults.standard.setValue(settings.hourceType.rawValue, forKey: Keys.settingsHours.rawValue)
        UserDefaults.standard.setValue(settings.tempType.rawValue, forKey: Keys.settingsTempt.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    private func loadSettings() {
        let hourseIntValue = UserDefaults.standard.value(forKey: Keys.settingsHours.rawValue) as? Int
        settings.hourceType = HourceType(rawValue: (hourseIntValue ?? 0)) ?? .h24
        
        let temIntValue = UserDefaults.standard.value(forKey: Keys.settingsTempt.rawValue) as? Int
        settings.tempType = TemprecherType(rawValue: temIntValue ?? 0) ?? .fahrenheit
    }
    
}
