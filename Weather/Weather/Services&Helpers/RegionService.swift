//
//  RegionService.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-05-30.
//

import Foundation

class RegionService {
    func getAvailibleCities() -> [City] {
        return [
            City(id: "1", nameCity: "Washington, D.C.", countryName: "United States"),
            City(id: "2", nameCity: "Toronto", countryName: "Canada"),
            City(id: "2", nameCity: "Tokyo", countryName: "Japan"),
            City(id: "2", nameCity: "Barcelona", countryName: "Spain"),
            City(id: "2", nameCity: "Bangkok", countryName: "Thailand"),
            City(id: "2", nameCity: "Kyiv", countryName: "Ukraine"),
            City(id: "2", nameCity: "New York City", countryName: "United States"),
            City(id: "2", nameCity: "Cairo", countryName: "Egypt"),
            City(id: "2", nameCity: "Madrid", countryName: "Spain"),
            City(id: "2", nameCity: "London", countryName: "United Kingdom"),
            City(id: "2", nameCity: "Los Angeles", countryName: "United States"),
            City(id: "2", nameCity: "Washington, D.C.", countryName: "United States"),
            City(id: "2", nameCity: "Toronto", countryName: "Canada"),
            City(id: "2", nameCity: "Tokyo", countryName: "Japan"),
            City(id: "2", nameCity: "Barcelona", countryName: "Spain"),
            City(id: "2", nameCity: "Bangkok", countryName: "Thailand"),
            City(id: "2", nameCity: "Kyiv", countryName: "Ukraine"),
            City(id: "2", nameCity: "New York City", countryName: "United States"),
            City(id: "2", nameCity: "Cairo", countryName: "Egypt"),
            City(id: "2", nameCity: "Madrid", countryName: "Spain"),
            City(id: "2", nameCity: "London", countryName: "United Kingdom"),
            City(id: "2", nameCity: "Washington, D.C.", countryName: "United States"),
            City(id: "2", nameCity: "Toronto", countryName: "Canada"),
            City(id: "2", nameCity: "Tokyo", countryName: "Japan"),
            City(id: "2", nameCity: "Barcelona", countryName: "Spain"),
            City(id: "2", nameCity: "Bangkok", countryName: "Thailand"),
            City(id: "2", nameCity: "Kyiv", countryName: "Ukraine"),
            City(id: "2", nameCity: "New York City", countryName: "United States"),
            City(id: "2", nameCity: "Cairo", countryName: "Egypt"),
            City(id: "2", nameCity: "Madrid", countryName: "Spain"),
            City(id: "2", nameCity: "London", countryName: "United Kingdom"),
            City(id: "2", nameCity: "Washington, D.C.", countryName: "United States"),
            City(id: "2", nameCity: "Toronto", countryName: "Canada"),
            City(id: "2", nameCity: "Tokyo", countryName: "Japan"),
            City(id: "2", nameCity: "Barcelona", countryName: "Spain"),
            City(id: "2", nameCity: "Bangkok", countryName: "Thailand"),
            City(id: "2", nameCity: "Kyiv", countryName: "Ukraine"),
            City(id: "2", nameCity: "New York City", countryName: "United States"),
            City(id: "2", nameCity: "Cairo", countryName: "Egypt"),
            City(id: "2", nameCity: "Madrid", countryName: "Spain"),
            City(id: "2", nameCity: "London", countryName: "United Kingdom")
        ]
    }
}
