//
//  WeatherForecastService.swift
//  Weather
//
//  Created by lvorozhbyt on 2021-06-06.
//

import UIKit

class WeatherForecastService {
    static var shared = WeatherForecastService()
    
    private init() {}
    
    func predictForecast(forToday date: Date, cityId: String, completion: ([Forecast]?) -> Void) {
        var items = [
            Forecast(temperature: 34, precipProbability: 90, humidity: 90, cloudCover: 90, windSpeed: 745),
            Forecast(temperature: 10, precipProbability: 90, humidity: 90, cloudCover: 90, windSpeed: 745),
            Forecast(temperature: 7, precipProbability: 90, humidity: 90, cloudCover: 90, windSpeed: 745),
            Forecast(temperature: 39, precipProbability: 90, humidity: 90, cloudCover: 90, windSpeed: 745),
            Forecast(temperature: 42, precipProbability: 90, humidity: 90, cloudCover: 90, windSpeed: 745)]
//        completion()
    }

//    func predictForecast(forWeek startDate: Date, cityId: String, completion: @escaping (ApiResult<[Movie]?>?) -> Void) {
//
//    }
}
