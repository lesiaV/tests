//
//  ViewController.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-13.
//

import UIKit

class MoviesViewController: UIViewController {
    private var service = MoviesNetworkingService()
    private var personalService = PersonNetworkingService()
    
    private var index = 0
    private var movies: [Movie]? = nil
    private var popularPersons: [Person]? = nil
    private let dispatchGroup = DispatchGroup()
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView? {
        didSet {
            registerCollectionCells()
            configurateCollationView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
        dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
            self?.collectionView?.reloadData()
        }
    }
    
    func loadData() {
        dispatchGroup.enter()
        service.fetchPopularMovie(page: 1) { [weak self] res in
            switch res {
            case .error(let errorString):
                print(errorString)
            case .success(let result):
                self?.movies = result.results
            }
            self?.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        personalService.fetchPopularPerson { [weak self] res in
            switch res {
            case .error(let errorString):
                print(errorString)
            case .success(let result):
                guard let self = self else {
                    return
                }
                
                self.popularPersons = result.results
            }
            self?.dispatchGroup.leave()
        }
    }
    
    // MARK: CollectionView helpers methods
    private func registerCollectionCells() {
        collectionView?.register(MoviesCollectionViewCell.self, forCellWithReuseIdentifier: "MoviesCollectionViewCell")
        collectionView?.register(UINib(nibName: "MoviesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MoviesCollectionViewCell")
        
        collectionView?.register(PersonHeaderCollectionViewCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "PersonHeaderCollectionViewCell")
        collectionView?.register(UINib(nibName: "PersonHeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "PersonHeaderCollectionViewCell")
    }
    
    private func configurateCollationView() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        //        if let collectionViewLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
        //            collectionViewLayout.itemSize = CGSize(
        //                width:(self.view.frame.size.width - 10) / 2,
        //                height: (self.view.frame.size.width - 10) / 2
        //            )
        ////            collectionViewLayout.headerReferenceSize = CGSize(width: collectionView?.frame.size.width ?? 0, height: 100)
        //        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
        } else {
            print("Portrait")
        }
    }
}

extension MoviesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as? MoviesCollectionViewCell,
              let movie = movies?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        cell.title.text = movie.title
        cell.subtitleLabel.text = movie.overview
        
        if let url = movie.backdropPath {
            let url = URL(string: "https://image.tmdb.org/t/p/w500//\(url)")!
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    print(error!)
                    return
                }
                
                DispatchQueue.main.async { [weak cell] in
                    if  let data = data, let image = UIImage(data: data) {
                        cell?.movieIcon.image = image
                    }
                }
                
            }).resume()
        }
        
        return cell
    }
    
}

extension MoviesViewController: UICollectionViewDelegate {
    
}

extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 120)
        //    }
        //
        //    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        //        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as! MoviesCollectionViewCell
        ////        header.setupView()
        //
        //        return header
        //        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            if  let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "PersonHeaderCollectionViewCell", for: indexPath) as? PersonHeaderCollectionViewCell {
                reusableview.items = popularPersons?.map({ person in
                    return PersonHeader(title: person.name, icon: nil)
                })
                
                return reusableview
            }
            
            
        default:
            return UICollectionReusableView()
        }
        return UICollectionReusableView()
    }
}


