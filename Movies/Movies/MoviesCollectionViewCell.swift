//
//  MoviesCollectionViewCell.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-14.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var movieIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
