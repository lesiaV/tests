//
//  Movie.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-14.
//

import UIKit

class Movie: Codable {
    var title: String
    var overview: String
    var backdropPath: String?
}
