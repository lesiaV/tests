//
//  ResultMovies.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-14.
//

import UIKit

struct ResultMovies: Codable {
    let page: Int
    let results: [Movie]
}
