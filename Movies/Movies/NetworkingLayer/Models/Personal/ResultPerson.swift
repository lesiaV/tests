//
//  ResultPerson.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-23.
//

import UIKit

class ResultPerson: Codable {
    let page: Int
    let results: [Person]
}
