//
//  Person.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-23.
//

import UIKit

struct Person: Codable {
    var id: Int
    var name: String
    var popularity: Double?
    var profilePath: String?
}
