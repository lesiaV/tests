//
//  Response.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-22.
//

import UIKit

struct ErrorResponse {
    var errorString: String
    var statusCode: Int
}

enum Response<T: Codable> {
    case success(T)
    case error(ErrorResponse)
}
