//
//  NewTokenResponse.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-29.
//

import UIKit

struct NewTokenResponse: Codable {
    var success: Bool
    var requestToken: String
    var expiresAt: String
}
