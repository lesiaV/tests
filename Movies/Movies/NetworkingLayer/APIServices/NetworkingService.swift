//
//  NetworkingService.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-16.
//

import UIKit
import Security

class NetworkingService {
    private let baseURLStr = "https://api.themoviedb.org/3"
        
     func fetchData<T: Codable>(
        with methodPath: String,
        queryParams: [String: String]?,
        bodyParams: [String: String]?,
        httpMethod: String,
        completionHandler: @escaping ((_ result: Response<T>) -> Void)
     ) {
       
        let session = URLSession.shared
        guard let url = configURLComponents(pathComponent: methodPath, queryItems: queryParams)?.url else {
            return
        }
                
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.httpBody = configRequestBody(params: bodyParams)
        
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                let statusCode = (response as? HTTPURLResponse)?.statusCode
                completionHandler(
                    .error(
                        ErrorResponse(
                            errorString: error.localizedDescription,
                            statusCode: statusCode ?? -1
                        )
                    )
                )
                return
            }
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            if let result = try? decoder.decode(T.self, from: data!) {
                completionHandler(.success(result))
            } else {
                completionHandler(.error(
                    ErrorResponse(
                        errorString: "Parsering",
                        statusCode: -1
                    )
                ))
            }
        }.resume()
    }
    
    // MARK: Configure Request params
   private func configURLComponents(
        pathComponent: String,
        queryItems: [String : String]?
    ) -> URLComponents? {
        guard let baseUrl = URL(string: baseURLStr) else {
            return nil
        }

        let url = baseUrl.appendingPathComponent(pathComponent)
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)

        guard let queryItems = queryItems else {
            return components
        }

        var items = [URLQueryItem]()
        for (key, value) in queryItems {
            let item = URLQueryItem(name: key, value: value)
            items.append(item)
        }

        components?.queryItems = items
        return components
    }
    
    private func configRequestBody(params: [String: String]?) -> Data? {
        guard let params = params else {
            return nil
        }

        return try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
    }
}
