//
//  PersonNetworkingService.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-23.
//

import UIKit

class PersonNetworkingService: NetworkingService {
    func fetchPopularPerson(complition: @escaping ((Response<ResultPerson>) -> Void)) {
        fetchData(
            with: "person/popular",
            queryParams: [
                "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5",
                "language": "en-US"
            ],
            bodyParams: nil,
            httpMethod: "GET",
            completionHandler: complition)
    }
}
