//
//  AuthRequestType.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-29.
//

import UIKit

enum AuthRequestType {
    case requestToken
    case login
    
    var apiPath: String {
        switch self {
        case .requestToken:
            return "/authentication/token/new"
        case .login:
            return "/authentication/token/validate_with_login"
        }
    }
    
    var body: [String : String]? {
        switch self {
        case .requestToken:
            return nil
        case .login:
            return nil
        }
    }
    
    var methodsType: String {
        switch self {
        case .requestToken:
            return "GET"
        case .login:
            return "POST"
        }
    }
    
    var queryParams:  [String : String]? {
        var queryParams = [
            "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5",
        ]
        
        switch self {
        case .requestToken, .login: break

        }
        
        return queryParams
    }
}
