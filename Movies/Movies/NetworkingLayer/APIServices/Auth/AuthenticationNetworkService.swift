//
//  AuthenticationNetworkService.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-23.
//

import UIKit

final class AuthenticationNetworkService: NetworkingService {
    
    func requestToken(complition: @escaping ((Response<NewTokenResponse>) -> Void)) {
        let authRequestType: AuthRequestType = .requestToken
        
        fetchData(
            with: authRequestType.apiPath,
            queryParams: authRequestType.queryParams,
            bodyParams: authRequestType.body,
            httpMethod: authRequestType.methodsType,
            completionHandler: complition)
    }
    
    func login(username: String, password: String, complition: @escaping ((Response<String>) -> Void)) {
        let body = [
            "username": "\(username)",
            "password": "\(password)",
            "request_token": ""
        ]
        
        fetchData(
            with: "/authentication/token/validate_with_login",
            queryParams: [
                "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5",
            ],
            bodyParams: body,
            httpMethod: "POST",
            completionHandler: complition)
    }
}
