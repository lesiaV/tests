//
//  MoviesNS.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-21.
//

import UIKit

class MoviesNetworkingService: NetworkingService {
    
    func fetchPopularMovie(page: Int, complition: @escaping ((Response<ResultMovies>) -> Void)) {
        fetchData(
            with: "movie/popular",
            queryParams: [
                "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5",
                "language": "en-US",
                "page" : "\(page)"
            ],
            bodyParams: nil,
            httpMethod: "GET",
            completionHandler: complition)
    }
    
    
    func fetchNowPlaying(page: Int, complition: @escaping (([Movie]?) -> Void)) {
//        configurateSessionTask(
//            with: "movie/now_playing",
//            queryParams: [
//                "api_key": "55957fcf3ba81b137f8fc01ac5a31fb5",
//                "language": "en-US",
//                "page" : "\(page)"
//            ],
//            bodyParams: nil,
//            httpMethod: "GET"
//        ) { data, response, error in
////            let decoder = JSONDecoder()
////            decoder.keyDecodingStrategy = .convertFromSnakeCase
////            let resultMovies = try? decoder.decode(ResultMovies.self, from: data!)
////            complition(resultMovies?.results)
//        }
    }
    
    
}
