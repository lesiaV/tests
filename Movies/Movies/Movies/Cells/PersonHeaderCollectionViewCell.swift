//
//  PersonHeaderCollectionViewCell.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-23.
//

import UIKit

struct PersonHeader {
    var title: String
    var icon: UIImage?
}

class PersonHeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var items: [PersonHeader]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView?.register(PersonCollectionViewCell.self, forCellWithReuseIdentifier: "PersonCollectionViewCell")
        collectionView?.register(UINib(nibName: "PersonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PersonCollectionViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension PersonHeaderCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "PersonCollectionViewCell", for: indexPath
        ) as? PersonCollectionViewCell,
        let person = items?[indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.titleLabel.text = person.title
        cell.iconImageView.image = person.icon
//        cell.iconImageView.set = {
////            iconImageView =
//        }
        return cell
    }
}

extension PersonHeaderCollectionViewCell: UICollectionViewDelegate {
    
}


let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func loadImageUsingCache(imgUrl: URL?) {
        guard let url = imgUrl else { return }

        // check cached image
        if let cachedImage = imageCache.object(forKey: imgUrl?.absoluteString as NSString? ?? "")  {
            self.image = cachedImage
            return
        }

        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style: .medium)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()
        activityIndicator.center = self.center

        // if not, download image from url
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if  let data = data, let image = UIImage(data: data), let imgUrlString = imgUrl?.absoluteString as NSString?  {
                    imageCache.setObject(image, forKey: imgUrlString)
                    self.image = image
                    activityIndicator.removeFromSuperview()
                }
            }

        }).resume()
    }
}
