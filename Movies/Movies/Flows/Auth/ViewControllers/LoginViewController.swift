//
//  LoginViewController.swift
//  Movies
//
//  Created by lvorozhbyt on 2021-06-29.
//

import UIKit

class LoginViewController: UIViewController {
    private let authService = AuthenticationNetworkService()
//   private let dispatchGroup = DispatchGroup()
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var paasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapper = UITapGestureRecognizer(target: self, action:#selector(endEditingTextField))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
        
        usernameTextField.delegate = self
        paasswordTextField.delegate = self
    }
    
    @IBAction func login(_ sender: Any) {
        authService.requestToken { res in
            
        }
    }
    
    @objc private func endEditingTextField(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
        if textField === usernameTextField {
            paasswordTextField.becomeFirstResponder()
        }
        return true
    }
}
